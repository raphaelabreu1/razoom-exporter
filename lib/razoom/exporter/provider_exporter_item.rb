module Razoom
  module Exporter
    class ProviderExporterItem
      NAME_INDEX = 0
      EMAIL_INDEX = 1
      PHONE_INDEX = 2
      LOCATION_INDEX = 3
      CORPORATE_NAME_INDEX = 4
      CNPJ_INDEX = 5
      CITY_INDEX = 6
      STATE_INDEX = 7
      DISTRICT_INDEX = 8
      WEBSITE_INDEX = 9
      RESPONSIBLE_NAME_INDEX = 10
      BANK_NAME_INDEX = 11
      BANK_BRANCH_INDEX = 12
      ACCOUNT_NUMBER_INDEX = 13
      BANK_RECIPIENT_INDEX = 14
      BANK_RECIPIENT_CPF_CNPJ_INDEX = 15


      attr_accessor :item

      def initialize
        @item = []
      end

      def method_missing(method_sym, *arguments, &block)
        if method_sym.to_s.start_with?('with_')
          const_name = "#{method_sym.to_s.gsub('with_', '').upcase}_INDEX"

          unless eval(const_name).nil?
            constant = eval(const_name)
            @item.insert(constant, arguments.first)
            self
          end
        else
          super
        end
      end
    end
  end
end