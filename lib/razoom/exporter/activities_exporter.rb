require 'csv'

module Razoom
  module Exporter
    class ActivitiesExporter
      FIELDS = ['Nome da empresa', 'Regular ou Privativo', 'Nome do Produto', 'Descrição', 'Pickup / Meeting Point',
                'Local de Pickup ou Meeting Point', 'País', 'UF', 'Cidade', 'Bairro', 'Endereço em que acontece o tour',
                'O que está incluso?', 'O que não está incluso?', 'Recomendações', 'Idade mínima', 'Antecedência para Reserva',
                'Antecedência para Cancelamento', 'Outras condições', 'Todos os dias', 'Segunda', 'Terça', 'Quarta',
                'Quinta', 'Sexta', 'Sabado', 'Domingo', 'Duração Aproximada', 'Participantes Min/Max',
                'Tipo de preço', 'Valor do tour', 'Ativo', 'Tipo (Transfer, Tour)']

      attr_reader :items

      def initialize
        @items = []
      end

      def add_item(item)
        @items << item.item
      end

      def execute
        puts 'Iniciando exportaçao de atividades'
        exported_count = 0

        CSV.open("#{Dir.getwd}/activities.csv", 'wb') do |csv|
          csv << FIELDS
          @items.each do |item|
            exported_count += 1
            csv << item
          end
        end

        puts "Finalizada exportaçao de atividades (atividades exportadas: #{exported_count})"
      end
    end
  end
end
