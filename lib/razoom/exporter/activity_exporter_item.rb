module Razoom
  module Exporter
    class ActivityExporterItem
      PROVIDER_NAME_INDEX = 0
      PRIVACY_TYPE_INDEX = 1
      NAME_INDEX = 2
      DESCRIPTION_INDEX = 3
      AVAILABLE_LOCATION_TYPE_INDEX = 4
      AVAILABLE_LOCATION_INDEX = 5
      COUNTRY_INDEX = 6
      STATE_INDEX = 7
      CITY_INDEX = 8
      DISTRICT_INDEX = 9
      LOCATION_INDEX = 10
      INFO_INCLUDED_INDEX = 11
      INFO_NOT_INCLUDED_INDEX = 12
      INFO_RECOMMENDED_INDEX = 13
      INFO_MINIMUM_AGE_INDEX = 14
      HOURS_IN_ADVANCE_INDEX = 15
      CANCEL_HOURS_IN_ADVANCE_INDEX = 16
      INFO_OTHER_REQUIREMENTS_INDEX = 17
      ALL_DAYS_INDEX = 18
      MONDAY_INDEX = 19
      TUESDAY_INDEX = 20
      WEDNESDAY_INDEX = 21
      THURSDAY_INDEX = 22
      FRIDAY_INDEX = 23
      SATURDAY_INDEX = 24
      SUNDAY_INDEX = 25
      DURATION_INDEX = 26
      MIN_MAX_PAX_INDEX = 27
      PRICE_TYPE_INDEX = 28
      PRICE_INDEX = 29
      ENABLED_INDEX = 30
      TYPE_INDEX = 31

      attr_accessor :item

      def initialize
        @item = []
      end

      def with_provider_name(value)
        # Só um exemplo de como funciona o method_missing
        @item.insert(PROVIDER_NAME_INDEX, value)
        self
      end

      def method_missing(method_sym, *arguments, &block)
        if method_sym.to_s.start_with?('with_')
          const_name = "#{method_sym.to_s.gsub('with_', '').upcase}_INDEX"

          unless eval(const_name).nil?
            constant = eval(const_name)
            @item.insert(constant, arguments.first)
            self
          end
        else
          super
        end
      end
    end
  end
end