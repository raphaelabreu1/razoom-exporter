require 'csv'

module Razoom
  module Exporter
    class ProvidersExporter
      FIELDS = ['Nome da Empresa', 'Email',	'Telefone', 'Endereço', 'Razão Social', 'CNPJ', 'Cidade', 'UF', 'Bairro', 'Website',
                'Responsável', 'Nome do Banco', 'Agência Bancária', 'Conta Corrente', 'Favorecido', 'CPF/CNPJ']

      attr_reader :items

      def initialize
        @items = []
      end

      def add_item(item)
        @items << item.item
      end

      def execute
        puts 'Iniciando exportaçao de fornecedores'
        exported_count = 0

        CSV.open("#{Dir.getwd}/providers.csv", 'wb') do |csv|
          csv << FIELDS
          @items.each do |item|
            exported_count += 1
            csv << item
          end
        end

        puts "Finalizada exportaçao de fornecedores (fornecedores exportados: #{exported_count})"
      end
    end
  end
end