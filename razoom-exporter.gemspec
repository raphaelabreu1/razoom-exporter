# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'razoom/exporter/version'

Gem::Specification.new do |spec|
  spec.name          = "razoom-exporter"
  spec.version       = Razoom::Exporter::VERSION
  spec.authors       = ["Raphael Abreu"]
  spec.email         = ["raphaelabreu1@gmail.com"]

  spec.summary       = %q{Formato de exportação para planilhas da Razoom.}
  spec.description   = %q{Adapta atividades e fornecedores para exportação no formato para planilhas da Razoom. Controle interno}
  spec.homepage      = "http://www.razoom.com.br"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.9"
  spec.add_development_dependency "rake", "~> 10.0"
end
