require 'spec_helper'

describe Razoom::Exporter do

  describe 'ActivityExporterItem' do
    it '#new' do
      expect(Razoom::Exporter::ActivityExporterItem.new).to_not be nil
    end

    it '#with_provider_name' do
      item = Razoom::Exporter::ActivityExporterItem.new.with_provider_name('Test name')
      expect(item.item).to eq(['Test name'])
    end

    it '#with_privacy_type' do
      item = Razoom::Exporter::ActivityExporterItem.new.with_provider_name('Test name').with_privacy_type('Regular')
      expect(item.item).to eq(['Test name', 'Regular'])
    end

    it '#with_invalid_method' do
      expect {
        Razoom::Exporter::ActivityExporterItem.new.with_provider_name('Test name').invalid_method('Invalid Method')
      }.to raise_error(NoMethodError)
    end
  end

  describe 'ProviderExporterItem' do
    it '#new' do
      expect(Razoom::Exporter::ProviderExporterItem.new).to_not be nil
    end

    it '#with_provider_name' do
      item = Razoom::Exporter::ProviderExporterItem.new.with_name('Test name')
      expect(item.item).to eq(['Test name'])
    end

    it '#with_privacy_type' do
      item = Razoom::Exporter::ProviderExporterItem.new.with_name('Test name').with_email('test@example.com')
      expect(item.item).to eq(['Test name', 'test@example.com'])
    end

    it '#with_invalid_method' do
      expect {
        Razoom::Exporter::ProviderExporterItem.new.with_name('Test name').invalid_method('Invalid Method')
      }.to raise_error(NoMethodError)
    end
  end
end
